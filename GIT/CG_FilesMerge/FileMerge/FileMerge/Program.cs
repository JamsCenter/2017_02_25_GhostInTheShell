﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace FileMerge
{
    class Program
    {

        [STAThread]
        static void Main(string[] args)
        {
            
            
            string currentDirectory = Directory.GetCurrentDirectory();
            Console.Out.WriteLine(">>>> Directory Path:" +currentDirectory );

            string[] csFiles = Directory.GetFiles(currentDirectory);

            string mergeFile = currentDirectory + "/" + "MergeFiles.cs";
            File.Delete(mergeFile);
            File.AppendAllText(mergeFile, "using System; using System.Linq; using System.IO; using System.Text; using System.Collections; using System.Collections.Generic; \n");
            for (int j = 0; j < args.Length; j++)
            {
                Console.Out.WriteLine("Ignore:" + args[j] + ".cs");
            }

            for (int i = 0; i < csFiles.Length; i++)
            {
                bool isFileToIgnore = false;

                for (int j = 0; j < args.Length; j++)
                {
                    if (-1 != csFiles[i].IndexOf(args[j] + ".cs"))
                        isFileToIgnore = true;
                }

                if (isFileToIgnore || -1!=csFiles[i].IndexOf("MergeFiles.cs"))
                {
                    Console.Out.WriteLine("> " + "isFileToIgnore ?" );

                }
                else  if ( (csFiles[i].LastIndexOf(".cs") == csFiles[i].Length - 3) )
                {
                    Console.Out.WriteLine("> " + csFiles[i]);
                    string[] lines = File.ReadAllLines(csFiles[i]);
                    for (int lCount = 0; lCount < lines.Length; lCount++)
                    {
                        while(lines[lCount].IndexOf("  ")!=-1)
                            lines[lCount] =  lines[lCount].Replace("  ", " ");
                        bool isCommentLine = lines[lCount].StartsWith("///") || lines[lCount].StartsWith("//") ||
                             lines[lCount].StartsWith(" ///") || lines[lCount].StartsWith(" //");
                        bool isAllowCommentLine = isCommentLine && (lines[lCount].StartsWith("///#") || lines[lCount].StartsWith(" ///#"));
                        bool isLibraryLine = lines[lCount].StartsWith("using");
                        bool isEmptyLine = lines[lCount].Length==0;
                        if ( isLibraryLine  )
                        { }
                        else if (isCommentLine && !isAllowCommentLine)
                        { }
                        else if (isEmptyLine)
                        { }
                        else
                        {
                            File.AppendAllText(mergeFile, lines[lCount] + "\n");
                        }
                    }
                }
            }
            

            var yourString = File.ReadAllText(mergeFile);

            Clipboard.SetText(yourString);
            System.Diagnostics.Process.Start(mergeFile);

    }
}
}


