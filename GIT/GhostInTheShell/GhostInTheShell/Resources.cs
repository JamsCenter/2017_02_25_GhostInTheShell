﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

    /// <summary>
    /// Resources Access
    /// </summary>
    class R
{
    #region ALIAS OF VERY USED CLASS;
    /// <summary>  Access data as raw as they was </summary>
    public static GivenData     Data { get { return GivenData.I;  } }
        /// <summary>  Ask to do somethink before the end of the turn (else pass turn).</summary>
    public static OrderStack    Order { get { return OrderStack.I; } }
    /// <summary>  Group of function to display game information. </summary>
    public static Display       Display { get { return Display.I; } }
    /// <summary> Delegate and access to notification on wath is happpening in the game.</summary>
    public static Overwatch Overwatch { get { return Overwatch.I; } }
    /// <summary> Delegate and access to notification on wath is happpening in the game.</summary>
    public static WarsSimulator Wars { get { return WarsSimulator.I; } }

    public static Arguments Args { get { return Arguments.I; } }


    public static TurnManagerInterface GameState = new TurnManagerNewbie();
    #endregion

    #region QUICK ACCESS TO VERY USED DATA;
    public static List<Troop>   Troops { get { return Data.GetTroops(); } }
    public static List<Factory> Factories { get { return Data.GetFactories(); } }
    public static List<FactoryActivity> Activities { get { return Overwatch.GetActivities(); } }
    public static List<Factory> EnemyFactories { get { return Filter.GetEnemy(Factories); } }
    public static List<Troop>   EnemyTroops { get { return Filter.GetEnemy(Troops); } }
    public static List<Factory> AllyFactories { get { return SF.KeepFactories(R.Factories, Factions.Ally); } }
    public static List<FightSimulation> Simulation { get { return Overwatch.GetSimulations(); } }
    public static List<FightSimulation.Result> SimuResults { get { return Overwatch.GetSimResults(); } }

    internal static void IncomingUnits(Factory o, out int ally, out int enemy)
    {
        SF.GetIncomingTroops(R.Troops, o, out ally, out enemy);
    }

    public static List<Troop>   AllyTroops { get { return SF.KeepTroops(R.Troops, PlayerFaction.Ally); } }
    
    public static List<Path>    Paths { get { return Data.GetPaths(); } }


    public static int           Turn { get { return GivenData.I.GetTurn(); } }
    public static Factory       RandomEnemy { get { return SF.GetRandomInRange(EnemyFactories); } }

    public static Path          StartPath { get { return Data.PathOf(StartOf(PlayerFaction.Ally).ID, StartOf(PlayerFaction.Enemy).ID);} }

    public static Path          LongestPath { get { return R.Paths.OrderByDescending(x => x.Distance).First(); } }

    public static int CurrentProduction { get { return R.Factories.Sum(f=>f.Production); } }
    public static int MaxProduction { get { return R.Factories.Count * 3; } }

    public static NukeManager Nuke { get { return NukeManager.I; } }

    public static ClaimMap Claim { get { return ClaimMap.I; } }

    public static List<Factory> Nearest(Factory factory) { return SF.GetNeightbourSortByDistance(factory); }
    #endregion

    #region QUICK ACCESS TO VERY USED METHODS
    public static Factory ID(int factoryID)
    {
            return Data.GetFactory(factoryID);
    }
    public static int Distance(Factory f1, Factory f2) {
        return Data.GetDistanceBetween(f1, f2);
    }

    public static Factory StartOf(PlayerFaction team) {
       return Data.GetStartup(team);
    }

    internal static int UnitsIn(List<Troop> list)
    { return SF.UnitsIn(list); }
    internal static int UnitsIn(List<Factory> list)
    { return SF.UnitsIn(list); }

    internal static List<Factory> Keep(List<Factory> factories, Factions team)
    {
        return SF.KeepFactories(factories, team);
    }
    internal static List<Factory> Remove(List<Factory> factories, Factions team)
    {
        return SF.RemoveFactories(factories, team);
    }
    internal static List<Troop> Keep(List<Troop> factories, PlayerFaction team)
    {
        return SF.KeepTroops(factories, team);
    }
    internal static List<Troop> Remove(List<Troop> factories, PlayerFaction team)
    {
        return SF.KeepTroops(factories, team);
    }

    internal static int ProductionOf(List<Factory> list)
    { return SF.ProductionOf(list); }
    internal static int ProductionOf(Factions faction)
    {
        return R.Factories.Where(f=>f.Is(faction)).Sum(f=>f.Production);
    }

    internal static List<Factory> Neighbour(Factory current, out int minRange)
    {
        return SF.GetNeighbour(R.Factories, current, out minRange);
    }

    #endregion
    // public static 
}

