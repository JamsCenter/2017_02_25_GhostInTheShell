﻿using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;


public class NukeManager : Singleton<NukeManager>
{
    public List<Nuke> LaunchedNuke { get; private set; }
    public int AllyNuke=2; public int EnemtNuke=2;
    public bool HaveNuke { get{ return AllyNuke >0; } }

    public List<Factory> HaveBeenTargeted = new List<Factory>();

    public NukeManager()
    {
        LaunchedNuke = new List<Nuke>();
    }
    public void DetectedNukeLunch(Nuke nuke)
    {
        if (nuke.IsAlly) AllyNuke--;
        else EnemtNuke--;
        LaunchedNuke.Add(nuke);
    }
    


    List<string> LastTurnBomb=new List<string>();
    internal void BombDetectedThisTurn(List<string> bombDetected)
    {
        foreach (string boomId in LastTurnBomb)
        {
            if (!bombDetected.Contains(boomId))
                NotifyBombExplosion(boomId);
        }
        LastTurnBomb = bombDetected;
    }

    private void NotifyBombExplosion(string boomId)
    {
        LaunchedNuke.Where(x => x.ID == int.Parse(boomId)).First().Exploded();
    }

    internal int LeftEstimation(Factory bigResource)
    {
        return Math.Max(10, bigResource.Units / 2);
    }

    internal bool HasBeenTargeted(Factory destination)
    {
       return  LaunchedNuke.Where(l => l.Destination == destination).Count()>0;
    }

    internal bool LaunchTo(Factory d)
    {

        if (R.Nuke.HaveBeenTargeted.Contains(d))
            return false;
            R.Order.AddBombAttack(R.Keep(R.Nearest(d), Factions.Ally).First(),d);
            R.Nuke.HaveBeenTargeted.Add(d);
        return true;
    }
}
public class Nuke
{


    int _nukeId;
    bool _owner;
    int _factoryFrom;
    int _factoryTo;

    int _arrivingTurn;
    int _launchedTurn;

    public Nuke(int id, int owner, int from, int to, int turnLeft, int currentTurn)
    {
        _nukeId = id;
        _owner = owner == 1;
        _factoryFrom = from;
        _factoryTo = to;
        _arrivingTurn = currentTurn + turnLeft;
        _launchedTurn = currentTurn;
        PossibleTargets = SF.SortFactoryWithMostProductive(R.AllyFactories);
    }

    public List<Factory> PossibleTargets;
    public bool IsAlly { get { return _owner; } }
    public Factory Origine { get { return R.ID(_factoryFrom); } }
    public Factory Destination { get { return IsAlly ? R.ID(_factoryTo) : null; } }

    public int RepopulationTurn { get { return _arrivingTurn + 5; } }
    public int TurnLeft(int turn)
    {
        int turnleft = _arrivingTurn - turn;
        return turnleft < 0 ? 0 : turnleft;
    }
    public enum NukeState { Incoming, NextTurn, Boom, Radiation, Outdated }
    public NukeState State(int turn)
    {

        if (turn < _arrivingTurn - 1) return NukeState.Incoming;
        if (turn > _arrivingTurn + 5) return NukeState.Outdated;
        if (turn > _arrivingTurn) return NukeState.Radiation;
        if (turn == _arrivingTurn) return NukeState.Boom;
        if (turn == _arrivingTurn - 1) return NukeState.NextTurn;
        return NukeState.Outdated;
    }


    public int ID
    {
        get
        {
            return _nukeId;
        }
    }

    public bool IsEnemy
    {
        get
        {
            return !IsAlly;
        }
    }

    public void  Exploded () { _arrivingTurn = R.Turn; }

    public static bool operator ==(Nuke a, Nuke b)
    {
        if (System.Object.ReferenceEquals(a, b))
        {
            return true;
        }

        if (((object)a == null) || ((object)b == null))
        {
            return false;
        }

        return a.ID == b.ID;
    }

    public static bool operator !=(Nuke a, Nuke b)
    {
        return !(a == b);
    }

    public override bool Equals(System.Object obj)
    {
        Nuke p = obj as Nuke;
        if ((object)p == null)
        {
            return false;
        }
        return base.Equals(obj) && p.ID == ID;
    }

    public bool Equals(Nuke p)
    {
        return base.Equals((Nuke)p) && p.ID == ID;
    }

    public override int GetHashCode()
    {
        return base.GetHashCode() ^ ID;
    }


}


