﻿using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

public class GivenData : Singleton<GivenData>
{
    public const int MINFACTORY = 7;
    public const int MAXFACTORY = 15;
    public const int MINLINK = 21;
    public const int MAXLINK = 105;
    public const int MINDISTANCE = 1;
    public const int MAXDISTANCE = 20;
    public const int MAXTURN = 200;


    /// Number of factory in the game
    int _factoryCount = 0;
     Dictionary<int, Factory> _factoriesList = new Dictionary<int, Factory>();
     int _pathCount = 0;
    List<Path> _pathsList = new List<Path>();
    List<Nuke> _nukeList = new List<Nuke>();


    Dictionary<int, Troop> _troopCurrentTurn = new Dictionary<int, Troop>();
     Dictionary<int, Troop> _troopCurrentPreviousTurn = new Dictionary<int, Troop>();

     Factory _enemyStart;
     Factory _ourStart;

    public  void SetInitialData(int factoryCount, int pathCount)
    {
        _factoryCount = factoryCount;
        _factoriesList = new Dictionary<int, Factory>();
        _pathCount = pathCount;
        _pathsList = new List<Path>();

    }
    
    public  void SetStartup(Factory factory, PlayerFaction team)
    {
        if (team == PlayerFaction.Ally)
            _ourStart = factory;
        else
            _enemyStart = factory;
    }
    public  Factory GetStartup(PlayerFaction team)
    {
        return team == PlayerFaction.Ally ? _ourStart : _enemyStart;
    }

    public  void AddPath(int factoryIdFrom, int factoryIdTo, int distance)
    {
        _pathsList.Add(
            new Path(factoryIdFrom, factoryIdTo, distance)
            );
    }

    public  Path GetLink(int linkId) { throw new Exception("TODO LATER"); }
    public  List<Path> GetPossibleLinks(int factoryId) { throw new Exception("TODO LATER"); }

    public  int GetFactoryCount() { return _factoryCount; }
    public  int GetPathCount() { return _pathCount; }

    public  Factory GetFactory(int factoryID)
    {
        if (_factoriesList.Count <= 0)
            Debug.LogError(" The Factories are not initialized. Please call the methode SetInitialData(..)");
        return _factoriesList[factoryID];
    }

    public  void AddFactory(int entityId)
    {
        _factoriesList.Add(entityId, new Factory(entityId));
    }
    public  List<Factory> GetFactories() { return new List<Factory>(_factoriesList.Values); }

    internal void AddNukeLaunch(int entityId, int owner, int from, int to, int turnLeft)
    {
        if ( ! _nukeList.Any(item => item.ID ==entityId)) { 
            _nukeList.Add(new Nuke(entityId, owner, from, to, turnLeft, GetTurn()));
        }
    }

    public  List<int> GetFactoriesID() { return new List<int>(_factoriesList.Keys); }

    public  void SetFactoryStateTo(int entityId, int owner, int affectedUnit, int production)
    {
        Factory fact = GetFactory(entityId);
        fact.UpdateState(owner, affectedUnit, production);
    }

    public  void AddTroopToCurrentTurn(int entityId, int owner, int from, int to, int units, int turnLeft)
    {
        Troop troop = new Troop(entityId,R.Turn, turnLeft);
        troop.SetTroopStateWithArguments(owner, from, to, units, turnLeft);
        _troopCurrentTurn.Add(entityId, troop);

    }

     int _turnIndex = -1;
    public  int GetTurn() { return _turnIndex; }

    public  void SetAsNewTurn()
    {
        _turnIndex++;

        _troopCurrentPreviousTurn = _troopCurrentTurn;
        _troopCurrentTurn = new Dictionary<int, Troop>();

    }

    public  int GetPathsCount()
    {
        return _pathsList.Count;
    }
    public  int GetTroopsCount()
    {
        return _troopCurrentTurn.Keys.Count;
    }

    public  int GetFactoriesCount()
    {
        return _factoriesList.Keys.Count;
        
    }

    public  List<Troop> GetTroops()
    {
        return new List<Troop>(_troopCurrentTurn.Values);
    }
    public  List<Troop> GetTroops(PlayerFaction team)
    {
        return (from t in new List<Troop>(_troopCurrentTurn.Values)
                where t.Is(team)
                select t).ToList();
    }

    public  List<Path> GetPaths()
    {
        return _pathsList;
    }

    public  Dictionary<int, List<Factory>> _factorySortedByNearestDistance = new Dictionary<int, List<Factory>>();

    public  void FirstTurnComputation()
    {
        //FACTORY NEIGHBOUR DEFINE
        List<Factory> factories = GetFactories();
        for (int i = 0; i < factories.Count; i++)
        {
            int id = factories[i].ID;
            List<Factory> factoriesSorted = SF.GetNeightbourSortByDistance(id);
            _factorySortedByNearestDistance.Add(id, factoriesSorted);

        }



    }

    internal void StopRecordingTurnTime()
    {
        throw new NotImplementedException();
    }

    public  List<Factory> GetNearestFactoriesOf(int factoryId)
    {
        return _factorySortedByNearestDistance[factoryId];
    }

    public  Path PathOf(int factId1, int factId2)
    {
        if (factId1 == factId2)
            return null;
        foreach (Path p in _pathsList)
        {
            if (p.Contain(factId1, factId2))
                return p;
        }
        return null;

    }
    public int GetDistanceBetween(Factory factId1, Factory factId2) {
        return GetDistanceBetween(factId1.ID, factId2.ID);
    }
    public  int GetDistanceBetween(int factId1, int factId2)
    {
        if (factId2 == factId1)
            return 0;
        return PathOf(factId1, factId2).Distance;
    }

    public  int DistanceBetweenStartup()
    {
        return GetDistanceBetween(_enemyStart, _ourStart);
    }
    

    public  List<Factory> GetEmpties()
    {
        List<Factory> f = GetFactories();
        return SF.RemoveUnproductifFactory(f, true);
    }

    public  Factory GetEqualsDeparture(Factory target, int turnDuration)
    {
       List<Factory> factories = I.GetNearestFactoriesOf(target.ID);
        for (int i = 0; i < factories.Count; i++)
        {
            if (factories[i].IsAlly && I.GetDistanceBetween(target.ID, factories[i].ID) == turnDuration) {
                return factories[i];
            }
        }
        return null;
    }

 

}
public class Timer
{
    public static long StartGame;
    public static long StartTurn;
    public static long MaxTimeByTurn = 100000000;
    public static long Time { get { return DateTime.Now.Ticks; } }
    public static long TimeSinceStart { get { return Time - StartGame; } }
    public static long TimeSinceStartTurn { get { return Time - StartTurn; } }
    public static long LastTurnDuration { get; private set; }
    public static double LastTurnPourcentUsed { get { return ((double)LastTurnDuration) / ((double)MaxTimeByTurn); } }
    public static void SetGameStart() { StartGame = Time; }
    public static void SetTurnStart() { StartTurn = Time; }
    public static void SetTurnEnd() { LastTurnDuration = Time - StartTurn; }
}