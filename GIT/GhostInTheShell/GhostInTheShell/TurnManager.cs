﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public delegate void TurnCall();
public interface TurnManagerInterface {

    //void WhenScriptStart(TurnCall toDo);
    //void WhenRecoverGameInfo(TurnCall toDo);
    //void WhenRecoverTurnInfo(TurnCall toDo);
    //void WhenBasicDataRecorded(TurnCall toDo);
    //void WhenSendingCommands(TurnCall toDo);

    /// <summary>  #1: Read the input from Console.ReadLine() before while(true) </summary>
    void ReadInitialConsoleInput();

    /// <summary>  #2: The Game Basic Data are initialized. The game is starting. </summary>
    void Start();

    /// <summary>  #3: Read the input from Console.ReadLine() before the turn start </summary>
    void BeforeTurnConsoleInput();

    /// <summary>  #4: The turn is starting </summary>
    void TurnStart();
    /// <summary>  #5: Is call once before the first turn start (1/200) </summary>
    void StartFirstTurn();
    /// <summary>  #6: Is call once before only on the last turn of the game (200/200) </summary>
    void StartOfLastTurn();
    /// <summary>  #7: Is call once by turn </summary>
    void Turn();

    /// <summary>  #8: Is call once before the first turn start (1/200) </summary>
    void EndingOfFirstTurn();
    /// <summary>  #9:  Is call once after the last turn of the game (200/200) before the Total End() </summary>
    void EndingOfLastTurn();
    /// <summary>  #10: The turn is finish </summary>
    void TurnEnd();


    /// <summary>  #11: The turn is finish </summary>
    void BeforeExecuteTurn();

    /// <summary>  #12: The turn is finish </summary>
    void ExecuteTurn();

    /// <summary>  #13: The turn is finish </summary>
    void AfterExecuteTurn();


    /// <summary>  #14: The game is totaly finished </summary>
    void End();
    /// <summary>  #0: The game is totaly finished </summary>
    void ScriptStart();

}

public class TurnManagerAbstract : TurnManagerInterface
{
    public virtual void ScriptStart() { }

    public virtual void ReadInitialConsoleInput() { }
    public virtual void Start() {}
    public virtual void BeforeTurnConsoleInput() { }
    public virtual void TurnStart() { }
    public virtual void StartFirstTurn() { }
    public virtual void StartOfLastTurn() { }
    public virtual void Turn() { }
    public virtual void EndingOfFirstTurn() { }
    public virtual void EndingOfLastTurn() { }
    public virtual void TurnEnd() { }
    public virtual void BeforeExecuteTurn() { }
    public virtual void ExecuteTurn() { }
    public virtual void AfterExecuteTurn() { }
    public virtual void End() { }

    //public TurnCall ToDoWhenScriptStart;
    //public void WhenScriptStart(TurnCall toDo)
    //{
    //    ToDoWhenScriptStart += toDo;
    //}

    //public TurnCall ToDoWhenRecoverInfo;
    //public void WhenRecoverGameInfo(TurnCall toDo)
    //{
    //    ToDoWhenRecoverInfo += toDo;
    //}

    //public TurnCall ToDoWhenRecoverTurnInfo;
    //public void WhenRecoverTurnInfo(TurnCall toDo)
    //{
    //    ToDoWhenRecoverTurnInfo += toDo;
    //}

    //public TurnCall ToDoWhenBaiscDataRecoverd;
    //public void WhenBasicDataRecorded(TurnCall toDo)
    //{
    //    ToDoWhenBaiscDataRecoverd += toDo;
    //}

    //public TurnCall ToDoWhenSedningCommand;
    //public void WhenSendingCommands(TurnCall toDo)
    //{
    //    ToDoWhenSedningCommand += toDo;
    //}

    
}

public class TurnManagerNewbie : TurnManagerAbstract {

    public override void Start()
    {
        Debug.Log("Hello World");
    }
    public override void TurnStart()
    {
        Debug.Log("Turn: " + R.Turn);
    }
    public override void Turn()
    {
        foreach (Factory f in SF.GetAllianceFactories())
        {
            for (int i = 0; i < f.Units - 1; i++)
            {
                Factory target = SF.GetRandomInRange(SF.RemoveUnproductifFactory(R.EnemyFactories));
                if (target != null)
                    R.Order.AddMove(f, target, 1);
            }
        }
    }
    public override void ExecuteTurn()
    {

        Debug.Log(R.Order.CurrentCMD);
    }
}