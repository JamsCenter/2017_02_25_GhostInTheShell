﻿using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;


public class Troop
{


     int _troopId;
     bool _owner;
     int _factoryFrom;
     int _factoryTo;

     int _unit;
    int _turnLeft;
    int _turnToArrived;
    int _turnCreated;
    int _turnDuration;


    public Troop(int troopId,int turnCreated, int turnLeft)
    {
        _troopId = troopId;
        _turnCreated = turnCreated;
        _turnToArrived = turnCreated + TurnLeft;
        _turnDuration = TurnLeft;
    }
    public void SetTroopStateWithArguments(int owner, int from, int to, int unit, int turnLeft)
    {
        _owner = owner == 1;
        _factoryFrom = from;
        _factoryTo = to;
        _unit = unit;
        _turnLeft = turnLeft;


    }

    public bool IsAlly { get { return _owner; } }
    public Factory Origine {   get{ return R.ID(_factoryFrom); } }
    public Factory Destination { get { return R.ID(_factoryTo); } }
    public int Units { get { return _unit; } }
    public int TurnLeft { get{ return _turnLeft; } }
    public int TurnArriving { get { return _turnCreated + _turnLeft; } }
    public int TurnDone { get { return _turnDuration - _turnLeft; } }
    public int TurnDuration { get { return _turnDuration; } }
    public bool IsNew { get { return _turnDuration == _turnLeft; } }
    public int ID
    {get
        {
            return _troopId;
        }
    }

    public bool IsEnemy
    {
        get {
            return !IsAlly;
        }
    }

    public static bool operator ==(Troop a, Troop b)
    {
        if (System.Object.ReferenceEquals(a, b))
        {
            return true;
        }

        if (((object)a == null) || ((object)b == null))
        {
            return false;
        }

        return a.ID==b.ID;
    }

    public static bool operator !=(Troop a, Troop b)
    {
        return !(a == b);
    }

    public override bool Equals(System.Object obj)
    {
        Troop p = obj as Troop;
        if ((object)p == null)
        {
            return false;
        }
        return base.Equals(obj) && p.ID == ID;
    }

    public bool Equals(Troop p)
    {
        return base.Equals((Troop)p) && p.ID == ID;
    }

    public override int GetHashCode()
    {
        return base.GetHashCode() ^ ID;
    }

    public bool Is(PlayerFaction team)
    {
        bool result = false;
        switch (team)
        {
            case PlayerFaction.Ally:
                if (IsAlly) result = true;
                break;
            case PlayerFaction.Enemy:
                if (IsEnemy) result = true;
                break;
            case PlayerFaction.Both:
                result = true;
                break;
            default:
                break;
        }
        return result;
    }
}


