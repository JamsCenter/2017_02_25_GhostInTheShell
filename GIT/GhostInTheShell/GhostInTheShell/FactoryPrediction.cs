﻿using System;
using System.Collections.Generic;
using System.Linq;


//ALGO AND TEST ON GOOGLE EXCEL: https://docs.google.com/spreadsheets/d/1vo35mi_jgPH5DYaBHorhXWw4rjWRg64ek0pYXl-E1yA/edit?usp=sharing
public class FightWithReinforcementSimulation {
    public int[] Reinforce { get; set; }
    public FightSimulation.Result Simulation { get; set; }
}




public class FightPredictionParams{
     int _factoryUnit;
     int _factoryProduction;
     Owner _owner;
     int [] _incomingAllyTroops;
     int [] _incomingEnemyTroops;
     int _totalTurn;

    public int      TotalTurn { get { return _totalTurn; } }
    public Owner    InitialOwner { get { return _owner; } }
    public int      StartUnit { get { return _factoryUnit; } }
    public int      Production { get { return _factoryProduction; } }
    

    public int      GetUnits(int index, PlayerFaction team) {
        return index >= _incomingAllyTroops.Length ? 0 : team == PlayerFaction.Ally ? _incomingAllyTroops[index] : _incomingEnemyTroops[index];
    }

    public FightPredictionParams(Factory target) {
        _factoryProduction = target.Production;
        _factoryUnit = target.Units;
        _owner = target.Owner;
        

        List<Troop> enemy = SF.GetTroopsTargeting(target, PlayerFaction.Enemy);
        List<Troop> ally = SF.GetTroopsTargeting(target, PlayerFaction.Ally);
       
        _totalTurn = Math.Max(ally.Count>0?ally.Max(x => x.TurnLeft):0, enemy.Count>0? enemy.Max(x => x.TurnLeft):0);
        _incomingAllyTroops = new int[_totalTurn];

        for (int i = 0; i < ally.Count; i++)
        {
            _incomingAllyTroops[ally[i].TurnLeft - 1] = ally[i].Units;
        }

        _incomingEnemyTroops = new int[_totalTurn];
        for (int i = 0; i < enemy.Count; i++)
        {
            _incomingEnemyTroops[enemy[i].TurnLeft - 1] = enemy[i].Units;
        }
    }

    public string DisplayFormat()
    {
        return string.Format("Params: Last Attack in {0}: Defence({1}), Ally({2}), Enemy({3})", TotalTurn, StartUnit, GetIncomingUnits(PlayerFaction.Ally), GetIncomingUnits(PlayerFaction.Enemy));
    }

    private object GetIncomingUnits(PlayerFaction team)
    {
        int count = 0;
        if (team == PlayerFaction.Ally || team == PlayerFaction.Both)
            count += _incomingAllyTroops.Sum(x => x);
        if (team == PlayerFaction.Enemy || team == PlayerFaction.Both)
            count += _incomingEnemyTroops.Sum(x => x);
        return count;
    }
}
public class FightSimulation {


    private FightPredictionParams _iniParams;
    public FightPredictionParams Params { get { return _iniParams; } }
    public Result LastResult;
    public Result LastResultNoProduction;
    private Factory f;
    public Factory Factory { get { return f; } }

    public FightSimulation(Factory f)
    {
        SetParams(f);
    }

    public void SetParams(Factory f) {
       this. f = f;
        _iniParams = new FightPredictionParams(f);
    }
    #region OLD
    //public FightWithReinforcementSimulation SimulateBestReinforcement( int maxUnity=20, int maxTurn=5 ) {
    //    Result bestResult=null;
    //    int [] bestReinforcement = new int[0];

    //    int[] reinforcementTest = new int[maxTurn];
    //    for (int iTurn = 0; iTurn < maxTurn; iTurn++)
    //    {

    //        for (int iMaxUnity = 0; iMaxUnity < maxUnity; iMaxUnity++)
    //        {
    //            for (int i = 0; i < maxUnity; i++)
    //                reinforcementTest[iTurn] = 0;
    //            reinforcementTest[iTurn] = iMaxUnity;


    //            Result fight = Simulate(reinforcementTest.Count,reinforcementTest);
    //            bool isBetter = false;
    //                if (fight.LastTurn.FactoryFightWinner == Owner.Ally) {
    //                    isBetter = bestResult == null || reinforcementTest.Sum() < bestReinforcement.Sum();

    //                if (isBetter) {
    //                    bestResult = fight;
    //                    bestReinforcement = reinforcementTest.ToArray();
    //                }


    //            }
    //        }

    //    }
    //    return new FightWithReinforcementSimulation() { Reinforce=bestReinforcement, Simulation = bestResult};
    //} 
    #endregion

    public Result Simulate(int turnNeeded, params int[] reinforcement)
    {
        LastResultNoProduction = Simulate(turnNeeded, false, reinforcement);
        return  LastResult = Simulate(turnNeeded,true, reinforcement);
    }
     public Result Simulate(int turnNeeded,bool withProd=true, params int[] reinforcement)
    {

        int turnToSimulate = 0; 
        if (turnToSimulate <= reinforcement.Length)
            turnToSimulate = reinforcement.Length;
        if (turnToSimulate <= turnNeeded)
            turnToSimulate = turnNeeded+1;
        if (turnToSimulate <= Params.TotalTurn)
            turnToSimulate = Params.TotalTurn;


        TurnResult[] turnResult = new TurnResult[turnToSimulate];
        FightPredictionParams p = Params;


        TurnResult firstTurn = new TurnResult(0, p.StartUnit, withProd ? p.Production:0, p.InitialOwner);
        if (turnToSimulate <= 0)
        {
            firstTurn.SimulateFight();
            return new Result(new TurnResult[] { firstTurn });

        }
        int[] resizedArray = new int[turnToSimulate];
        if (reinforcement.Length <= turnToSimulate)
        {
            reinforcement.CopyTo(resizedArray, 0);
        }
        else {
            resizedArray = reinforcement.Take(turnToSimulate).ToArray();
        }
        reinforcement = resizedArray;


        firstTurn.SetIncomingTroops(p.GetUnits(0, PlayerFaction.Ally), reinforcement[0], p.GetUnits(0, PlayerFaction.Enemy));

        //Simulate fight
        firstTurn.SimulateFight();
        turnResult[0] = firstTurn;



        TurnResult previousTurn = firstTurn;
        //foreach left: take result create a new turn and simulate;
        for (int i = 1; i < turnToSimulate; i++)
        {
            int producted = previousTurn.Owner == Owner.Neutral || !withProd ? 0 : p.Production;
            TurnResult turn = new TurnResult(i, previousTurn.FactoryFightResult+producted, withProd?p.Production:0, previousTurn.FactoryFightWinner);
            turn.SetIncomingTroops(p.GetUnits(i, PlayerFaction.Ally), reinforcement[i], p.GetUnits(i, PlayerFaction.Enemy));
            turn.SimulateFight();
            turnResult[i] = turn;
            previousTurn = turn;
        }

        return LastResult=new Result(turnResult);
    }
    

    public class Result {
        public TurnResult[] _turn;
        public Result(TurnResult[] result) {
            _turn = result;
        }

        public TurnResult GetResult(int index) { return _turn[index]; }
        public TurnResult LastTurn { get { return _turn[_turn.Length - 1]; } }
        public TurnResult FirstTurn { get { return _turn[0]; } }

        public int UnitAt(int turn) { return _turn[turn].FactoryFightResult; }
        public Owner OwnerAt(int turn) { return _turn[turn].FactoryFightWinner; }

        public Owner OwnerAtStart { get { return FirstTurn.FactoryFightWinner; } }
        public int UnitsAtStart { get { return FirstTurn.FactoryFightResult; } }

        public Owner Winner { get { return LastTurn.FactoryFightWinner; } }
        public int UnitsLeft { get { return LastTurn.FactoryFightResult; } }

        public IEnumerable<TurnResult> Turns { get { return _turn; } }

        public void Display() {
            D.L(DisplayFormat());
            for (int i = 0; i < _turn.Length; i++)
            {
                D.L(_turn[i].DisplayFormat());
            }

        }
        public string DisplayFormat()
        {
            return (string.Format("Simulation: (Owner:{0}|{1} Winner:{2}|{3}", FirstTurn.Owner, FirstTurn.Unit, LastTurn.FactoryFightWinner, LastTurn.FactoryFightResult ));        
        }

        internal TurnResult GetTurn(int turn)
        {
            if (turn <= 0 || turn >= _turn.Length)
                return null;
            return _turn[turn];
        }
    }

    public class TurnResult {

        public TurnResult(int index, int unit, int production, Owner owner) {
            TurnIndex = index;
            Unit = unit;
            Production = production;
            Owner = owner;
        }

        public void SetIncomingTroops(int allyUnits, int allyReinforcement, int enemyUnit) {
            IncomingAlly = allyUnits;
            Reinforcement = allyReinforcement;
            IncomingEnemy = enemyUnit;
        }
        //GIVEN DATA
        public int TurnIndex;
        public int Unit;
        public int Production;
        public Owner Owner;

        public int IncomingAlly;
        public int IncomingEnemy;
        public int Reinforcement;


        
        //COMPUTED DATA
        public int IncomingTroopsFightResult;
        public PlayerFaction IncomingFightWinner;

        public int FactoryFightResult;
        public Owner FactoryFightWinner;

        public void SimulateFight() {

            //SIM INCOMING BATTLE
            SimulateTroopsBattle(out IncomingTroopsFightResult, out IncomingFightWinner, IncomingAlly, IncomingEnemy,   Reinforcement);
            

            //SIM FACTORY  BATTLE
            SimulatreFacotryBattle(IncomingTroopsFightResult, IncomingFightWinner, out FactoryFightResult, out FactoryFightWinner);



        }

        public void SimulateTroopsBattle(out int leftUnits, out PlayerFaction winner, int allyUnits, int enemyUnits,  int reinforcement)
        {
      
            int fightResult = (allyUnits + reinforcement) - enemyUnits ;
            leftUnits = Math.Abs(fightResult);
            winner = fightResult >= 0 ? PlayerFaction.Ally : PlayerFaction.Enemy;
        }
        public void SimulatreFacotryBattle(int incomingUnits, PlayerFaction unitsFaction, out int unitLeft, out Owner newOwner)
        {
            if (incomingUnits == 0)
            {
                unitLeft = Unit;
                newOwner = Owner;
            }
            else { 
                unitLeft = 0;
                bool isReinforcement = (Owner == Owner.Ally && unitsFaction == PlayerFaction.Ally)
                        || (Owner == Owner.Enemy && unitsFaction == PlayerFaction.Enemy);
                if (isReinforcement)
                {
                    unitLeft = incomingUnits + Unit;
                    newOwner = Owner;
                }
                else
                {
                    int fightResult = Unit - incomingUnits;
                    unitLeft = Math.Abs(fightResult);
                    if (fightResult >= 0)
                        newOwner = Owner;
                    else newOwner = unitsFaction == PlayerFaction.Ally ? Owner.Ally : Owner.Enemy;
                }
            }
        }
        public string DisplayFormat()
        {
            return (string.Format("Turn {0}, Current:{1} with {2} (A {3} vs E {4})  Troops Fight {7}({8}) Future Owner: {5} with {6}", TurnIndex, Owner, Unit, IncomingAlly, IncomingEnemy , FactoryFightWinner, FactoryFightResult, IncomingFightWinner, IncomingTroopsFightResult));

        }
 
    }

}