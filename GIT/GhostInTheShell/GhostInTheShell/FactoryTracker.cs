﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class Overwatch :Singleton<Overwatch>{


    public void SetUpWith(List<Factory> factories)
    {
        foreach (Factory f in factories)
        {
            _activities.Add(f.ID, new FactoryActivity(f));
            FightSimulation fight = new FightSimulation(f);
            _simulations.Add(f.ID, fight);
            _simulationsResult.Add(f.ID, fight.Simulate(5));

        }
    }

    public Dictionary<int, FactoryActivity> _activities = new Dictionary<int, FactoryActivity>();

    public List<FactoryActivity> GetActivities() { return _activities.Values.ToList(); }
    
    public Dictionary<int, FightSimulation> _simulations = new Dictionary<int, FightSimulation>();
    public Dictionary<int, FightSimulation.Result> _simulationsResult = new Dictionary<int, FightSimulation.Result>();

    public List<FightSimulation> GetSimulations() { return _simulations.Values.ToList(); }
    public List<FightSimulation.Result> GetSimResults() { return _simulationsResult.Values.ToList(); }

    public void SimulateAll(int miniTurn=5) {
        foreach (Factory f in R.Factories)
        {
            //USE IF OVERWATCH FACTORY IS STABLE ;)
            _simulationsResult[f.ID]= _simulations[f.ID].Simulate(miniTurn);

        }
    }
    /// <summary>
    /// Notify what happened during the turn and need to be watch:
    /// Factory lost, win
    /// Bomb appereing...
    /// </summary>
    //public class Overwatch : Singleton<Overwatch>
    //{
    //    //public void IsNukeDetected() { throw new Exception("Could be implemented?"); }
    //    //public List<Factory> GetCapturedFactory() { throw new Exception("Could be implemented?"); }
    //    //public List<Factory> GetUpdatedFactory() { throw new Exception("Could be implemented?"); }
    //    //public List<Factory> GetDestressSignal() { throw new Exception("Could be implemented?"); }

    //    //public List<Factory> GetDisabledFactory() { throw new Exception("Could be implemented?"); }
    //}

    //public class Wave
    //{
    //    public List<Factory> _factoriesState;
    //    public List<Troop> _troopState;
    //    //  public WarsStat _warsStat;
    //}
}

public class FactoryActivity
{
    public enum FactoryType { Unknow, Link, Leaf, BaseStation, MainBase, Defense }
    public FactoryType Type = FactoryType.Unknow;
    public Factory _linkedFactory;
    public Queue<int> _sendTroop =new Queue<int>(4);
    public Queue<int> _requiredTroop = new Queue<int>(4);
    public Queue<int> _lastUnityValue = new Queue<int>(4);
    private Factory f;

    public FactoryActivity(Factory f)
    {
        _linkedFactory = f;
        for (int i = 0; i < 4; i++)
        {
            _sendTroop.Enqueue(0);
            _requiredTroop.Enqueue(0);
            _lastUnityValue.Enqueue(0);

        }
    }

    public int SendedTroop { get { return _sendTroop.Last(); } 
            private set { _sendTroop.Dequeue(); _sendTroop.Enqueue(value); } } 
    public int RequiredTroop
    {
        get { return _requiredTroop.Last(); }
        private set { _requiredTroop.Dequeue(); _requiredTroop.Enqueue(value); }
    }
    public int Units
    {
        get { return _lastUnityValue.Last(); }
        private set { _lastUnityValue.Dequeue(); _lastUnityValue.Enqueue(value); }
    }
    public double UnitsAverage
    {
        get { return _lastUnityValue.Average(); }
    }
    public double SendAverage
    {
        get { return _lastUnityValue.Average(); }
    }
    public double RequiredAverage
    {
        get { return _requiredTroop.Average(); }
    }

    public bool IsUnitStable { get { return _lastUnityValue.All(x => x == UnitsAverage); } }

    public void OnTurnStart()
    {
        int currentUnit = _linkedFactory.Units;
            _lastUnityValue.Dequeue();
        _lastUnityValue.Enqueue(currentUnit);
        List<Troop> troops = R.Troops;
        int st=0, rt=0;
        foreach (Troop t in troops)
        {
            if (t.IsNew && t.Origine == _linkedFactory) st++;
            if (t.IsNew && t.Destination == _linkedFactory) rt++;

        }
         SendedTroop=st;
         RequiredTroop=rt;
        Type = DefineFactoryType();

    }

    public FactoryType DefineFactoryType() {
        double st = SendAverage, rt = RequiredAverage;
        double ratio = st / rt;
        if (Units > 30.0) return FactoryType.MainBase;
        if (Units > 20.0) return  FactoryType.BaseStation;
        if (ratio > 5.0) return FactoryType.Leaf;
        if (ratio < 0.8) return FactoryType.Defense;
        return FactoryType.Link;


    }
}

