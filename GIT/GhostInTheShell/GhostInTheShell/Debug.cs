﻿using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Debug (Alias)
/// </summary>
static class D
{
    public static void L(string message) { Debug.Log(message); }
    public static void LE(string message) { Debug.LogError(message); }
}
static class Debug
{
    public static void LogError(string message) { Console.Error.WriteLine("ERROR:" + message); }
    public static void Log(string message, bool withStartLine = true) { Console.Error.WriteLine((withStartLine ? "> " : "") + message); }
}
