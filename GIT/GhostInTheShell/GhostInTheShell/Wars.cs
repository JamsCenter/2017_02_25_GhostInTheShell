﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class WarsSimulator: Singleton<WarsSimulator>
{


    public FightSimulation.Result Simulate(Factory f, int turnNeeded = -1)
    {
        FightPredictionParams paramsPrediciton;
        return Simulate(f, out paramsPrediciton,turnNeeded);

    }
    public FightSimulation.Result Simulate(Factory f, out FightPredictionParams parms, int turnNeeded=-1)
    {
        FightSimulation simulation = new FightSimulation(f);
        parms = simulation.Params;
        return simulation.Simulate( turnNeeded );
    }


    public FightSimulation.Result[] Simulate(List<Factory> f) {
        FightSimulation.Result[] result = new FightSimulation.Result[f.Count];
        for (int i = 0; i < f.Count; i++)
        {
            result[i] = Simulate(f[i]);
        }
        return result;
    }
}
