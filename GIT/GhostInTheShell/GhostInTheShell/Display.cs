﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class Display :Singleton<Display>
    {
        public void Print(string message)
        {
            string [] splited = message.Split('\n');
            for (int i = 0; i < splited.Length; i++)
                D.L(splited[i]);
        }
    
        public string Format(Troop o)
        {
           return string.Format("{0}: {1}- {2} in {3} ->{4}", o.ID, o.Origine, o.Units, o.TurnLeft, o.Destination);
        }
        
        public string Format(Troop[] o)
        {
            string d = string.Format(" \nTroops({0}):\n", o.Length);
            foreach (Troop t in o)
            {d += Format(t) +"\n";}
            return d;
        }

    public string Format(Path o)
    {
        return string.Format("{0}<-{1}->{2}", o.FactoryOne.ID, o.Distance, o.FactoryTwo.ID);
    }


    public string Format(Path [] o)
    {

        string d = string.Format(" \nPaths({0}):\n", o.Length);
        foreach (Path p in o)
        { d += Format(p) + "\n"; }
        return d;
    }
    public string Format(Factory o, bool plus=true)
    {
        string factory = string.Format("\nID {0}: {1}({2})", o.ID, o.Units, o.Production);
        if (!plus)
            return factory;

        string near = "";
        foreach (Factory f in R.Nearest(o))
        {
            near += string.Format(" {0}({1})", f.ID, R.Distance(o, f));
        }
        int ally, enemy;
        R.IncomingUnits(o, out ally, out enemy);
        return string.Format("{0} Near {1} A{2} E{3}) ", factory, near, ally, enemy);

    }
    public string Format(Factory[] o, bool plus = true)
    {
        string factories = "";
        foreach (Factory f in o)
        {
            factories += Format(o, true);

        }

        return string.Format("\n Factories({0})\nStart: {1}\n{2}", R.Data.GetFactoriesCount(), R.StartPath.Distance, factories);


    }
    public void Print(Arguments o) { Print(Format(o)); }
    public string Format(Arguments o,bool troops=false, bool nukes=true) {
        string r = "";
        for (int i = 0; i < o.Lines.Count; i++)
        {

            if ((nukes && o.Lines[i].Contains("BOMB")) || (troops && o.Lines[i].Contains("TROOP")))
                r += "-- " + o.Lines[i] + "\n";

        }
        return r;
    }

    public string Format(GivenData o)
    {
        string r="";
        r += string.Format(
            " GAME STATE: {0}/{1}  Time:{2}({3:0.00}%)\n",
            R.Data.GetTurn() + 1, GivenData.MAXTURN, Timer.LastTurnDuration, Timer.LastTurnPourcentUsed);
        r += string.Format(
            " MAP DENSITY: {0}/{1}\n",
            R.Factories.Count, GivenData.MAXFACTORY);
        r += string.Format(
            " MAP SIZE: {0}/{1}\n",
            R.LongestPath.Distance, GivenData.MAXDISTANCE);
        r += string.Format(
            " RESOURCE DENSITY: {0}/{1}\n",
            R.CurrentProduction, R.MaxProduction);
        r += string.Format(
            " RESOURCE USED: A {0} N {1} E {2}\n",
            R.ProductionOf(Factions.Ally), R.ProductionOf(Factions.Neutral), R.ProductionOf(Factions.Enemy));
        r += string.Format(
            " STARTS PROXIMITY: ({0})\n",
            Format(R.StartPath));

        List<Troop> ally = R.AllyTroops, enemy = R.EnemyTroops;
        List<Factory> fally = R.AllyFactories , fneutral =R.Keep(R.Factories,Factions.Neutral), fenemy = R.EnemyFactories;
        
        r += string.Format(
            " WARS - TROOPS A {0}|{2} E{1}|{3} - FACTORY UNITY A {4} N {5} E{6}\n",
            ally.Count, enemy.Count, R.UnitsIn(ally), R.UnitsIn(enemy), fally.Sum(x => x.Units),fneutral.Sum(x => x.Units), fenemy.Sum(x => x.Units));
        r += string.Format(
            " WARS TOTALS: A {0} N {1} E{2}\n",
             R.UnitsIn(ally) + fally.Sum(x => x.Units), fneutral.Sum(x => x.Units), R.UnitsIn(enemy) + fenemy.Sum(x => x.Units));
        r += string.Format(
                    " NUKE : {0}\n",
                     Format(NukeManager.I));


        return r;
    }

    private object Format(NukeManager o)
    {
        string r = "";
        foreach (Nuke nuke in o.LaunchedNuke)
        {
            r += string.Format("\n N-{0} {1} ", nuke.ID, nuke.State(R.Turn)) ;
        }
        return r;
    }
    public void Print(FightSimulation o) { Print(Format(o)); }
    public string Format(FightSimulation o, bool plus = true)
    {
        string r = "Simuation on " + o.Factory;
        r += "With Defense:" + Format(o.LastResult);
        r += "Without defense:" + Format(o.LastResultNoProduction);
        return r;
    }
    public void Print(FightSimulation.Result o) { Print(Format( o)); }
    public string Format(FightSimulation.Result o, bool plus = true)
    {
        string result = string.Format("\nResult: Owner {0}|{1} Winner {2}|{3}", o.FirstTurn.Owner, o.FirstTurn.Unit, o.LastTurn.FactoryFightWinner, o.LastTurn.FactoryFightResult);
        if (!plus)
        return result;
        string turnDetail = "";
        foreach (FightSimulation.TurnResult t in o.Turns)
            turnDetail += Format(t) + "\n";
        return result + turnDetail;

    }
    public string Format(FightSimulation.TurnResult o) {
        return (string.Format("\nTurn {0} ||{1} -> {2} (A {3} vs E {4}) -> {7}({8}) =  {5} with {6}", o.TurnIndex, o.Owner, o.Unit, o.IncomingAlly, o.IncomingEnemy, o.FactoryFightWinner, o.FactoryFightResult, o.IncomingFightWinner, o.IncomingTroopsFightResult));

    }

    public void Print(ClaimMap o) { Print(Format(o)); }
    public string Format(ClaimMap o)
    {
        List<Claim> claims = R.Claim.AllClaims();
        string map = string.Format("\nClaimed Map ({0}): ", claims.Count);
        foreach (Claim f in claims)
        {
            map += Format(f);
        }

        return map; 
    }
    public string Format(Claim o) {
        return (string.Format("\nClaim on {0} of {1} at {3} with {2} priority ", o.Where, o.RequiredUnit, o.Priority ,o.WhenToApply));
    }
}



