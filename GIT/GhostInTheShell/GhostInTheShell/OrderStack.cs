﻿using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;


public class MoveOrder
{
    private int troupNumber;

    public MoveOrder(Factory from, Factory to, int units)
    {
        From = from;
        To = to;
        this.Units = units;
    }

    public Factory From { get; set; }
    public Factory To { get; set; }
    public int Units { get; set; }
}


public class OrderStack : Singleton<OrderStack>
{
    public List<string> actions = new List<string>();
    public List<MoveOrder> moves = new List<MoveOrder>();

    public  void ClearAction() {
        actions.Clear();
        moves.Clear();
    }


    public int UnitsSendTo(Factory f) {
        return moves.Where(x => x.To == f).Sum(x => x.Units);
    }

    public  void AddWait()
    {
        actions.Add("WAIT");
    }

    public void AddMove(Factory from, Factory to, int troupNumber)
    {
        actions.Add(string.Format("MOVE {0} {1} {2}", from.ID, to.ID, Math.Max(troupNumber, 0) ));
        moves.Add(new MoveOrder(from, to, troupNumber));
    }
    public void AddBombAttack(Factory from, Factory to)
    {
        actions.Add(string.Format("BOMB {0} {1}", from.ID, to.ID));
    }
    public void AddImprove(Factory factory )
    {
        actions.Add(string.Format("INC {0}", factory.ID));
    }
    public void AddMessage(string message)
    {
        actions.Add(string.Format("MSG {0} ",message));
    }

    public string CurrentCMD { get
        {
            string cmds = "";
            foreach (string cmd in actions)
            {
                cmds += cmd + ";";
            }
            if (cmds.Length > 0)
                cmds = cmds.Remove(cmds.Length - 1);
            else cmds = "Wait";

            return cmds;
        } }
        public  void ExecuteAction()
        {
            string cmd = CurrentCMD;
            Console.WriteLine(cmd);
            Debug.Log("Request Sent: " + cmd);

        }
}