﻿using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;


public class Path
{
    int _factoryOne;
    int _factoryTwo;
    int _distance;


    public Path(int fromFactory, int toFactory, int distance)
    {
        _factoryOne = fromFactory;
        _factoryTwo = toFactory;
        _distance = distance;
    }


    public Factory      FactoryOne { get { return R.ID(_factoryOne); } }
    public Factory      FactoryTwo { get { return R.ID(_factoryTwo); } }
    
    public Factory[]    Factories { get { return new Factory[] { R.ID(_factoryOne), R.ID(_factoryTwo) }; } }
    public int          Distance { get { return _distance; } }



    public bool Contain(int factoryId)
    {
        return _factoryOne == factoryId || _factoryTwo == factoryId;
    }
    public Factory GetOtherSide(int factId)
    {
        return _factoryOne == factId ? FactoryTwo : FactoryOne;
    }

    public bool Contain(int factId1, int factId2)
    {
        return (_factoryOne == factId1 && _factoryTwo == factId2) || (_factoryOne == factId2 && _factoryTwo == factId1);
    }

    public static bool operator ==(Path a, Path b)
    {
        if (System.Object.ReferenceEquals(a, b))
        {
            return true;
        }

        if (((object)a == null) || ((object)b == null))
        {
            return false;
        }

        return a.Contain(b._factoryOne, b._factoryTwo);
    }

    public static bool operator !=(Path a, Path b)
    {
        return !(a == b);
    }
    public override bool Equals(System.Object obj)
    {
        // If parameter cannot be cast to ThreeDPoint return false:
        Path p = obj as Path;
        if ((object)p == null)
        {
            return false;
        }

        // Return true if the fields match:
        return base.Equals(obj) && p.Contain(_factoryOne, _factoryTwo);
    }

    public bool Equals(Path p)
    {
        // Return true if the fields match:
        return base.Equals((Path)p) && p.Contain(_factoryOne, _factoryTwo);
    }

    public override int GetHashCode()
    {
        return base.GetHashCode() ^ int.Parse(_factoryOne+"00"+_factoryTwo);
    }
    
}

//public class GeneratePaths {

//    public void DebugTest()
//    {
//        Factory from = R.StartOf(PlayerFaction.Ally),
//            to=R.StartOf( PlayerFaction.Enemy);
//        PathCondition [] validIf = new PathCondition[] { new SafeTerritory(), new MaxLenght() { Lenght = R.Distance(from, to) } };
//        PathCondition [] exitIf = new PathCondition[] { new FromTo() { From = from, To = to } };
//        foreach (ComposedPath path in GetPathsFor(validIf, exitIf))
//        {

//        }
//    }

//    public List<ComposedPath> GetPathsFor( PathCondition[] valideCondition, PathCondition[] exitCondition) {
//        List<ComposedPath> validePath = new List<ComposedPath>();

//        ExploreAllPath(out validePath, ref valideCondition, ref exitCondition);

//        return validePath;
//    }

//    private void ExploreAllPath(out List<ComposedPath> validePath, ref PathCondition[] valideCondition, ref PathCondition exitCondition)
//    {
//        Stack<Factory> exploringStack = new Stack<Factory>();
//        while 

//    }
//}

//public abstract class PathCondition {
//    public abstract bool IsValid(List<Factory> factoriesPath);

//}
//public class SafeTerritory : PathCondition
//{
//    public Factory From,To;
//    public override bool IsValid(List<Factory> factoriesPath)
//    {
//        List<Factory> newList = factoriesPath.ToList();
//        newList.RemoveAt(newList.Count - 1);
//        return newList.Any(x => x.IsAlly || (x.IsNeutral && x.Units==0 ) || x == From || x == To);
//    }
//}
//public class MaxLenght : PathCondition
//{
//    public int Lenght = 0;
//    public override bool IsValid(List<Factory> factoriesPath)
//    {
//        return SF.Lenght(factoriesPath) <= Lenght;
//    }
//}
//public class FromTo : PathCondition
//{
//    public Factory From, To;
//    public override bool IsValid(List<Factory> factoriesPath)
//    {
//        return factoriesPath.Count>1 && From==factoriesPath[0] && To == factoriesPath[factoriesPath.Count-1];
//    }
//}

//public class ComposedPath {
//    public Path _directPath;
//    public List<Factory> _factories = new List<Factory>();
//    public int Lenght {
//        get { int l=0;
//            Factory previous = _factories[0];
//            for (int i = 1; i < _factories.Count; i++)
//            {
//                l += R.Distance(previous, _factories[i]);
//                previous = _factories[i];
//            }
//            return l;
//        }
//    }


//}


//#region Path management Factory
//public class RomePaths
//{
//    public static Dictionary<int, ShortestWayNode> GetMap(List<Factory> factories, Factory target, int avoidArmyOf = 20)
//    {
//        if (target == null)
//            return null;
//        Dictionary<int, ShortestWayNode> map = new Dictionary<int, ShortestWayNode>();
//        List<Factory> leaves = new List<Factory>();
//        Dictionary<int, Factory> isOut = new Dictionary<int, Factory>();
//        Explore( factories, target, ref leaves, ref isOut, ref map);

//        return map;
//    }

//    private static void Explore(List<Factory> factories, Factory target, ref List<Factory> leaves, ref Dictionary<int, Factory> isOut, ref Dictionary<int, ShortestWayNode> map)
//    {
//        int turnCount = 0;
//        int costCount = 0;
//        //Element à traiter
//        List<Factory> currentList = new List<Factory>();
//        //les prochains qui seront traiter le tours suivant
//        List<Factory> nextList = new List<Factory>();
//        // les feuilles de l'élément traiter courant.
//        List<Factory> leavesList = new List<Factory>();
//        currentList.Add(target);

//        while (currentList.Count > 0)
//        {
//            //Chaque tour, il y a un Factory en plus à marcher
//            turnCount++;

//            //Les éléments en cours ne doivent pas être réutiliser
//            foreach (Factory current in currentList)
//                isOut.Add(current.ID, current);
//            //Pour chaque élément récupérer les feuilles et créer un lien avec le nombre de ressources pour arrivés à destination
//            foreach (Factory current in currentList)
//            {
//                leavesList.Clear();
//               GetFactoryLeaf(current, ref leavesList, ref nextList, ref isOut);
//               foreach (Factory leaf in leavesList)
//                {
//                    AddLeafToMap(current, leaf, turnCount, ref map);
//                }
//            }
            
//            ClearCurrentAndInverse(ref currentList, ref nextList);
//        }

//    }

//    private static void ClearCurrentAndInverse(ref List<Factory> currentList, ref List<Factory> nextList)
//    {
//        currentList.Clear();
//        List<Factory> tmp = currentList;
//        currentList = nextList;
//        nextList = tmp;
//    }
//    /// <summary>
//    /// J'ajouter les feuilles d'exploration à la carte.
//    /// L'idée est que chaque case doit possèder des liens vers c'est parent du tour avant.
//    /// Et le nombre de ressource pour arriver au point voulu
//    /// </summary>
//    /// <param name="parent"> The direction where to go</param>
//    /// <param name="leaf">La Factory de feuille à ajoute à la map</param>
//    /// <param name="currentCost">Le cout total pour arrivé à destination</param>
//    /// <param name="map">La carte actuelle</param>
//    private static void AddLeafToMap(Factory parent, Factory leaf, int currentCost, ref Dictionary<int, ShortestWayNode> map)
//    {
//        if(! map.ContainsKey(leaf.ID))
//            map.Add(leaf.ID, new ShortestWayNode(leaf));
//        CostNodeFactory nz = new CostNodeFactory(parent, R.Distance(parent, leaf));
//        swn.Add(nz);
//    }

//    public static LeafCondition leafCondition = new AvoidBigArmyCondition() { BigArmy = 20 };
//    public abstract class LeafCondition
//    {
//        public abstract bool IsValide(Factory parent, Factory leaf);
//        //public abstract bool IsObligatory(Factory parent, Factory leaf);
//    }
//    public class AvoidBigArmyCondition : LeafCondition
//    {
//        public int BigArmy = 20;
//        public override bool IsValide(Factory parent, Factory leaf)
//        {
//            return leaf.Units < BigArmy;
//        }

//    }

//    /// <summary>
//    /// Je récupère les feuilles de l'élément courant qui ne sont pas figer
//    /// </summary>
//    /// <param name="current">Branche</param>
//    /// <param name="leaves">Feuilles à traiter si pas fixe</param>
//    /// <param name="next">List qui contine toutes le feuille traiter ce tour pour les utilisers au prochain</param>
//    /// <param name="isOut"> les éléments déjà traité  et donc fixe </param>
//    private static void GetFactoryLeaf(Factory current, ref List<Factory> leaves, ref List<Factory> next, ref Dictionary<int, Factory> isOut)
//    {

//        foreach (Factory linkToCurrent in SF.GetNeightbourSortByDistance(current))
//            if (!isOut.ContainsKey(linkToCurrent.ID))
//            {
//                leaves.Add(linkToCurrent);
//                if (!next.Contains(linkToCurrent))
//                    next.Add(linkToCurrent);

//            }
//    }



//    public static Queue<int> GetPath(ShortestWayNode way)
//    {
//        Queue<int> queue = new Queue<int>();

//        int nextId;
//        CostNodeFactory nextNode;
//        try
//        {
//            int antiStack = 0;
//            do
//            {
//                antiStack++;
//                nextNode = way.GetNextNode();
//                nextId = nextNode.Factory.ID;
//                queue.Enqueue(nextId);
//                //   Debug.DebugLog("P: " + nextId+" L:"+lenght);
//            } while (nextNode !=null && antiStack < 50);

//        }
//        catch (Exception) { D.L("Oups"); }
//        return queue;
//    }

//    public static Queue<int> GetPath(Factory target, Factory from)
//    {
//        Dictionary<int, ShortestWayNode> mapPath = GetMap(target);
//        return GetPath(mapPath[from.ID]);
//    }
//}

//public class ShortestWayNode
//{
//    public Factory Factory;
//    public List<CostNodeFactory> directions = new List<CostNodeFactory>();


//    public ShortestWayNode(Factory Factory)
//    {
//        this.Factory = Factory;
//    }
//    public void Add(CostNodeFactory node)
//    {
//        if (node != null)
//            directions.Add(node);
//    }

//    public Factory GetNextFactory()
//    {
//        int minCost = 10000;
//        Factory next = null;
//        foreach (CostNodeFactory nz in directions)
//            if (nz.cost < minCost)
//            {
//                next = nz.Factory;
//                minCost = nz.cost;
//            }
//        return next;
//    }
//    public CostNodeFactory GetNextNode(bool withRandom = true)
//    {
//        int minCost = 10000;
//        List<CostNodeFactory> next = new List<CostNodeFactory>();
//        foreach (CostNodeFactory nz in directions)
//        {
//            if (nz.cost < minCost)
//            {
//                next.Clear();
//                minCost = nz.cost;
//            }
//            if (nz.cost == minCost)
//            {
//                next.Add(nz);
//            }
//        }
//        if (next.Count == 0)
//            return null;
//        if (!withRandom)
//            return next[0];
//        int randomInt = new Random(DateTime.Now.Millisecond).Next();
//        return next[randomInt % next.Count];


//    }


//}
//public class CostNodeFactory
//{
//    public Factory Factory;
//    public int cost;
//    public CostNodeFactory(Factory Factory, int cost)
//    {
//        this.Factory = Factory;
//        this.cost = cost;
//    }
//}
//#endregion