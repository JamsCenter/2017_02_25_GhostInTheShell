﻿using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

public enum PlayerFaction { Ally, Enemy , Both }
public enum Factions { Ally, Enemy, Neutral, Players, All, EnemyAndNeutral  }
public enum Owner { Ally, Neutral, Enemy }
public enum ProductionLevel:int { None = 0, Small = 1, Medium = 2, Big = 3}
public enum FightWinner { None, Ally, Neutral, Enemy }


public enum MapProductionState :int { Empty=0, AlmostEmpty = 1, HalfEmpty = 2, AllFactoryWorking=3, AlmostFull = 4, Full = 5 }
public enum MapTerritoryState :int { Start = 0, FirstWaves = 1, AllFactoryTaken = 2, NoNeutralAnyMore = 3, Losing=9, Winning = 10 }
public enum MapType { Small, Normal, Large}