﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


    class Day7_ClaimAI : TurnManagerAbstract
{
    
    public override void StartFirstTurn()
    {

    }

    public override void Turn()
    {

        foreach (FightSimulation r in R.Simulation.Where(x=>x.Factory.IsAlly))
        {
            R.Display.Print(R.Display.Format(r));

        }

        List<FightSimulation> sims = R.Simulation.Where(s => s.Factory.IsAlly).ToList();
        foreach (FightSimulation s in sims)
        {
            D.L("Def:" + s);
            FightSimulation.TurnResult nextTurn = s.LastResultNoProduction.GetTurn(2);
            if (nextTurn.FactoryFightWinner == Owner.Enemy)
            {
                R.Display.Print(R.Display.Format(s.LastResultNoProduction));
                Claim c = new Claim("Defense of " + s.Factory.ID, s.Factory, R.Turn, s.Factory.Units, ClaimPriority.NotNegociable);
                R.Claim.AddClaim(c);

            }

        }

        
        foreach (Factory f in R.AllyFactories)
        {
            int units = R.Claim.UnitNotClaimed(f);
            R.Claim.AddClaim(new Claim("Central station", f, R.Turn, units,ClaimPriority.Low));
            Factory target = GivenData.I.GetFactory(0);
            if (f!=target)
                R.Order.AddMove(f,target , units);
        }

        R.Display.Print(R.Claim);
        D.L(""+ R.Claim.existingClaim.Count);
    }

    private FightSimulation.TurnResult GetSimNeedOf(Factory factory, int turn, bool withProduction)
    {
        List<FightSimulation> sims = R.Simulation.Where(s => s.Factory==factory).ToList();
        if(!withProduction) return sims[0].LastResultNoProduction.GetTurn(turn);
        else return sims[0].LastResult.GetTurn(turn);
    }
}

public abstract class Commander {

// LET CLAIM AND FIGHT

}