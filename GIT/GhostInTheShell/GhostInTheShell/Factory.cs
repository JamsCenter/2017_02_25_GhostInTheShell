﻿using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;


public class Factory
{
    public Factory(int id)
    {
        _factoryID = id;
    }
    public void UpdateState(int owner, int affecteUnit, int production)
    {
        _owner = owner;
        _affectedUnit = affecteUnit;
        _factoryProduction = production;
    }


    int _factoryID;
    public int ID
    {
        get{
            return _factoryID;
        }
    }

    int _owner;
    public Owner Owner
    {
        get{
            if (_owner == 0) return Owner.Neutral;
            if (_owner == 1) return Owner.Ally;
            return Owner.Enemy;
        }
    }

    int _affectedUnit;
    public int Units
    {
        get
        {
            return _affectedUnit;
        }
    }

    int _factoryProduction;
    public int Production
    {
        get{
            return _factoryProduction;
        }
    }

    public bool IsAlly
    {
        get
        {
            return Owner == Owner.Ally;
        }
    }
    public bool IsEnemy
    {
        get
        {

            return Owner == Owner.Enemy;
        }
    }

    public bool IsNeutral
    {
        get
        {
            return Owner == Owner.Neutral;
        }
    }


    public bool Is(Factions selection) {

        bool result=false;
        switch (selection)
        {
            case Factions.Ally:
                if (IsAlly) result = true; break;
            case Factions.Enemy:
                if (IsEnemy) result = true; break;
            case Factions.Neutral:
                if (IsNeutral) result = true;
                break;
            case Factions.Players:
                if (IsAlly ||IsEnemy) result = true;
                break;
            case Factions.EnemyAndNeutral:
                if (IsEnemy || IsNeutral) result = true;
                break;
            case Factions.All:
            default:
                return false;
        }
        return result;

    }


    public static bool operator ==(Factory a, Factory b)
    {
        if (System.Object.ReferenceEquals(a, b))
        {
            return true;
        }
        
        if (((object)a == null) || ((object)b == null))
        {
            return false;
        }

        return a.ID == b.ID;
    }

    public static bool operator !=(Factory a, Factory b)
    {
        return !(a == b);
    }


    public override bool Equals(System.Object obj)
    {
        Factory p = obj as Factory;
        if ((object)p == null)
        {
            return false;
        }
        
        return base.Equals(obj) && p.ID == ID;
    }

    public bool Equals(Factory p)
    {
        return base.Equals((Factory)p) && p.ID == ID;
    }

    public override int GetHashCode()
    {
        return base.GetHashCode() ^ ID;
    }
    public override string ToString()
    {
        return "F(" + ID + ")";
    }
}

