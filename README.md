# Ghost In The Shell
This git is a commun folder to archived our progression on the contest "Ghost In The Cell" of coding game.  
https://www.codingame.com/contests/ghost-in-the-cell  
You can find here code for the IA of Codingame contest.  
  
    
Discord Channel:  
https://discord.gg/2JCAPnX   
## Index
- Program: Contains Main() and initialisation
- R: Resources with access to shortcut, alias and most general resource
- GivenData: Containt all raw information of the game given by the Console.Read()
- SF: Containt all static fonction to sort, remove, get, filter...
- Order: Allow to stack the order to send at the end of the turn
- Filter: Receive lists to sort or filter the information in it

## Beans
- Factory: id, troop, production
- Troop: id, owner, from, to, troops, turn left
- Paths: from, to, distance

## Tools
Fight Simulator: https://docs.google.com/spreadsheets/d/1vo35mi_jgPH5DYaBHorhXWw4rjWRg64ek0pYXl-E1yA/edit?usp=sharing  
CS Files merger for Codingame: https://github.com/JamsCenter/2017_02_25_GhostInTheShell/raw/master/GIT/Tools/FileMerge.rar  
